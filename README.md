# Backend Project

This is the backend server for a Software-as-a-Service (SaaS) solution that allows customers to manage shipments and delivery partners to view and update shipment status. The backend is developed using Node.js with TypeScript and MongoDB as the database.

## Table of Contents
- [Introduction](#introduction)
- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
- [Project Structure](#project-structure)
- [Configuration](#configuration)
- [API Endpoints](#api-endpoints)
- [Authentication](#authentication)
- [Testing](#testing)
- [Logging](#logging)
- [Error Handling](#error-handling)

## Introduction
The SaaS backend is responsible for handling various functionalities, including customer management (on-boarding and off-boarding customers), delivery partner management, shipment management (create, update, and delete shipments), and shipment status update by delivery partners. It also implements authentication and authorization mechanisms to ensure secure access to the APIs.

## Features
- On-board and off-board customers
- On-board and off-board delivery partners
- Create, update, and delete shipments
- View and update shipment status by delivery partners
- Authentication and authorization using JWT tokens
- Request validation and error handling
- Unit tests
- Logging API activities

## Requirements
- Node.js (>=12.x)
- MongoDB (>=4.x)

## Installation
1. Clone the repository:
```bash
git clone https://gitlab.com/tarakeswararao.marla/backend-assessment.git
```

2. Install dependencies:
```bash
cd backend
npm install
```
3. Set up the MongoDB database and update the configuration as needed.

4. Run the development server:
```bash
npm run dev
```

The server will start running on http://localhost:8080.

## Project Structure
The project follows a standard folder structure for easy navigation and separation of concerns. The main folders are as follows:

- `src`: Contains the source code of the application.
   - `controllers`: Contains the controllers that handle the business logic for various API endpoints.
   - `middlewares`: Contains custom middleware functions for request validation and authentication.
   - `models`: Defines the Mongoose schemas for the MongoDB database.
   - `routes`: Contains the route handlers for different API endpoints.
   - `helpers`: Contains utility functions and constants.
- `tests`: Contains the unit tests.

## API Endpoints
The backend provides the following API endpoints:

- `POST /api/admin/login`:  Login for admins.
- `POST /api/customers/create`: On-board a new customer (Admin access required).
- `DELETE /api/customers/:id`: Off-board a customer by ID (Admin access required).
- `GET /api/customers`: Retrieve all customers (Admin or delivery partner access required).
- `POST /api/delivery-partners/create`: On-board a new delivery partner (Admin access required).
- `DELETE /api/delivery-partners/:id`: Off-board a delivery partner by ID (Admin access required).
- `GET /api/delivery-partners`: Retrieve all delivery partners (Admin or delivery partner access required).
- `POST /api/customers/login`:  Login for Customers.
- `POST /api/delivery-partners/login`:  Login for Delivery Partners.
- `POST /api/shipments/create`: Create a new shipment (Customer access required).
- `PUT /api/shipments/:id`: Update the status of a shipment by ID (Delivery partner access required).
- `DELETE /api/shipments/:id`: Delete a shipment by ID (Customer access required).

## Authentication
The backend uses JSON Web Tokens (JWT) for authentication. When users (customers, delivery partners, or admins) log in, they receive a JWT token, which they should include in the `Authorization` header of subsequent requests to access protected routes.

## Testing
Unit tests for the APIs are provided in the `tests` folder. To run the tests, use the following command:

```bash
npm test
```
The tests are written using Mocha, Chai, and Sinon.

## Logging
API activities are logged using a logging mechanism implemented in the backend. 

## Error Handling
The backend implements proper error handling for various scenarios, including invalid requests, database errors, and server-side issues. Errors are returned with appropriate HTTP status codes and descriptive error messages.
