import chai from 'chai';
import jwtUtils from '../src/helpers/jwt';

const expect = chai.expect;

describe('Token Utility Functions', () => {
  describe('generateToken', () => {
    it('should generate a valid JWT with the provided payload', () => {
      const payload = { userId: 'user123' };
      const token = jwtUtils.generateToken(payload);

      expect(token).to.be.a('string');
      expect(token).to.not.be.empty;
    });
  });

  describe('verifyToken', () => {
    it('should return the decoded payload for a valid token', () => {
      const payload = { userId: 'user123' };
      const token = jwtUtils.generateToken(payload);

      const decodedPayload: any = jwtUtils.verifyToken(token);
      expect(decodedPayload.userId).to.deep.equal(payload.userId);
    });

    it('should throw an error for an invalid token', () => {
      const invalidToken = 'invalid-token';
      
      // Wrapping the verification in a function to test the error
      const verifyFunction = () => jwtUtils.verifyToken(invalidToken);
      expect(verifyFunction).to.throw(Error, 'Invalid or expired token');
    });

    it('should throw an error for an expired token', (done) => {
      const payload = { userId: 'user123' };
      
      // Generate a token that expires in the past (1 millisecond ago)
      const expiredToken = jwtUtils.generateToken(payload, { expiresIn: '1ms' });

      // Wait for the token to expire before attempting verification
      setTimeout(() => {
        // Wrapping the verification in a function to test the error
        const verifyFunction = () => jwtUtils.verifyToken(expiredToken);
        expect(verifyFunction).to.throw(Error, 'Invalid or expired token');
        done();
      }, 10);
    });
  });
});
