import mongoose, { Document, Schema } from 'mongoose';

export interface CustomerDoc extends Document {
    name: string;
    email: string;
    phoneNumber: string;
    password: string;
    textPassword: string;
}

const customerSchema = new Schema<CustomerDoc>({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    phoneNumber: { type: String, required: true },
    password: { type: String, required: true },
    textPassword: { type: String, required: true }
});

export default mongoose.model<CustomerDoc>(
    'Customer', customerSchema
);