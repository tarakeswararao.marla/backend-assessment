import mongoose, { Document, Schema } from 'mongoose';

export interface ShipmentDoc extends Document {
    customerId: mongoose.Types.ObjectId;
    deliveryPartnerId: mongoose.Types.ObjectId;
    sourceLocation: string;
    targetLocation: string;
    status: string;
}

const shipmentSchema = new Schema<ShipmentDoc>({
    customerId: { type: Schema.Types.ObjectId, required: true },
    deliveryPartnerId: { type: Schema.Types.ObjectId, required: true },
    sourceLocation: { type: String, required: true },
    targetLocation: { type: String, required: true },
    status: { type: String, enum: ['pending', 'in_progress', 'delivered'], default: 'pending' }
});

export default mongoose.model<ShipmentDoc>(
    'Shipment', shipmentSchema
);