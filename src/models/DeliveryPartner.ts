import mongoose, { Document, Schema } from 'mongoose';

export interface DeliveryPartnerDoc extends Document {
    name: string;
    email: string;
    phoneNumber: string;
    password: string;
    textPassword: string;
}

const customerSchema = new Schema<DeliveryPartnerDoc>({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    phoneNumber: {type: String, required: true },
    password: { type: String, required: true },
    textPassword: { type: String, required: true }
});

export default mongoose.model<DeliveryPartnerDoc>(
    'DeliveryPartner', customerSchema
);