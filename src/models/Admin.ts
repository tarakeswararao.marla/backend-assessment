import mongoose, {Document, Schema} from "mongoose";

export interface AdminDoc extends Document {
    email: string;
    password: string;
    type: string;
}

const adminSchema = new Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    type: { type: String, enum: ['admin'], default: 'admin' }
});

export default mongoose.model<AdminDoc>(
    'Admin', adminSchema
);