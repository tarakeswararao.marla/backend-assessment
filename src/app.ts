import express, { 
    Application, Request, Response, NextFunction
} from 'express';
import mongoose, { ConnectOptions } from 'mongoose';
import cors from 'cors';
import morgan from 'morgan';
import errorHandlerMiddleware from './middlewares/errorHandlerMiddleware';
import adminRoutes from './routes/adminRoutes';
import customerRoutes from './routes/customerRoutes';
import deliveryPartnerRoutes from './routes/deliveryPartnerRoutes';
import shipmentRoutes from './routes/shipmentRoutes';

const app: Application = express();

//Middleware
app.use(express.json());
app.use(cors());
app.use(morgan('dev'));

// Database connection
const dbURI = 'mongodb://localhost:27017/db';
mongoose.connect(dbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
} as ConnectOptions).then(() => console.log('Connected to MongoDb'))
    .catch((err: any) => console.error('MongoDB connection error:', err));

// Routes
app.use('/api/admin', adminRoutes);
app.use('/api/customers', customerRoutes);
app.use('/api/delivery-partners', deliveryPartnerRoutes);
app.use('/api/shipments', shipmentRoutes);

app.use(errorHandlerMiddleware);

export default app;