import Admin from "../models/Admin";
import bcrypt from 'bcryptjs';

export default async () => {
    try {
        const email = 'admin@gmail.com';
        const password = 'test@123';
        const type = 'admin';
        const hashedPassword = await bcrypt.hash(password, 10);

        const superAdmin = new Admin({
            email,
            password: hashedPassword,
            type
        });
        await superAdmin.save();
        console.log('initial data loaded');
    } catch (error) {
    }
};