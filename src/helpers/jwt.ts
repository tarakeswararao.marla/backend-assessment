import jwt from 'jsonwebtoken';

const SECRET_KEY = 'secret-key';

const generateToken = (payload: object, options:object = { expiresIn: '1d' }) => {
    return jwt.sign(payload, SECRET_KEY, options);
};

const verifyToken = (token: string) => {
    try {
        return jwt.verify(token, SECRET_KEY);
    } catch (err) {
        // Handle invalid or expired tokens here
        throw new Error('Invalid or expired token');
    }
};

export default {
    generateToken,
    verifyToken
};
