import { RequestHandler } from 'express';
import bcrypt from 'bcryptjs';
import DeliveryPartner from '../models/DeliveryPartner';
import { statusCodes } from '../helpers/utils'
import jwt from '../helpers/jwt';
import generator from 'generate-password';

const create: RequestHandler = async (req, res) => {
    try {
        const { name, email, phoneNumber } = req.body;

        const existingDeliveryPartner = await DeliveryPartner.findOne({ email });
        if(existingDeliveryPartner) {
            return res.status(statusCodes.BAD_REQUEST).json({
                message: 'Email already registered'
            });
        }

        // const password = generator.generate({
        //     length: 10,
        //     numbers: true
        // });
        const password = 'test@123';
        const hashedPassword = await bcrypt.hash(password, 10);

        const deliveryPartner = new DeliveryPartner({
            name,
            email,
            phoneNumber,
            password: hashedPassword,
            textPassword: password
        });
        await deliveryPartner.save();

        res.status(statusCodes.CREATED).json({
            message: 'Delivery partner registered successfully',
            password
        });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
}

const get: RequestHandler = async (req, res) => {
    try {
        const deliveryPartners = await DeliveryPartner.find({});
        res.status(statusCodes.OK).json({ deliveryPartners });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

const remove: RequestHandler = async (req, res) => {
    try {
        const deliveryPartner = await DeliveryPartner.findById(req.params.id);

        if(!deliveryPartner) {
            return res.status(statusCodes.NOT_FOUND).json({
                message: 'Delivery Partner not found'
            });
        }

        await deliveryPartner.deleteOne();

        res.json({ message: 'Delivery Partner removed successfully', deliveryPartner });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

const login: RequestHandler = async (req, res) => {
    try {
        const { email, password } = req.body;

        const deliveryPartner = await DeliveryPartner.findOne({ email });
        if(!deliveryPartner) {
            return res.status(statusCodes.UNAUTHORIZED).json({
                message: 'Invalid credentials'
            });
        }

        const isPasswordValid = await bcrypt.compare(password, deliveryPartner.password);
        if(!isPasswordValid) {
            return res.status(statusCodes.UNAUTHORIZED).json({
                message: 'Invalid credentials'
            });
        }

        const token = jwt.generateToken({ deliveryPartnerId: deliveryPartner._id, type: 'delivery-partner' });
        res.json({
            message: 'Login successful',
            token
        });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

export default {
    create,
    get,
    remove,
    login
};