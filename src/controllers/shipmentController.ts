import { RequestHandler } from 'express';
import Shipment from '../models/Shipment';
import { statusCodes } from '../helpers/utils';

const create: RequestHandler = async (req, res) => {
    try {
        let {
            customerId,
            deliveryPartnerId,
            sourceLocation,
            targetLocation
        } = req.body;
        
        if(!customerId) {
            customerId = req.headers.customerId;
        }

        const shipment = new Shipment({
            customerId,
            deliveryPartnerId,
            sourceLocation,
            targetLocation
        });

        await shipment.save();
        
        res.status(statusCodes.CREATED).json({
            message: 'Shipment created successfully',
            shipment
        });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

const get: RequestHandler = async (req, res) => {
    try {
        const condition:any = {};
        if(req.headers.customerId) {
            condition.customerId = req.headers.customerId;
        }
        if(req.headers.deliveryPartnerId) {
            condition.deliveryPartnerId = req.headers.deliveryPartnerId;
        }

        const shipments = await Shipment.find(condition);

        res.json({ shipments });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

const update: RequestHandler = async (req, res) => {
    try {
        const shipment = await Shipment.findById(req.params.id);

        if(!shipment) {
            return res.status(statusCodes.NOT_FOUND).json({
                message: 'Shipment not found'
            });
        }

        shipment.status = req.body.status;
        await shipment.save();

        res.json({ message: 'Shipment updated successfully', shipment });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

const remove: RequestHandler = async (req, res) => {
    try {
        const shipment = await Shipment.findById(req.params.id);

        if(!shipment) {
            return res.status(statusCodes.NOT_FOUND).json({
                message: 'Shipment not found'
            });
        }

        await shipment.deleteOne();

        res.json({
            message: 'Shipment deleted successfully'
        });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

export default {
    create,
    get,
    update,
    remove
};