import { RequestHandler } from 'express';
import bcrypt from 'bcryptjs';
import { statusCodes } from '../helpers/utils';
import Admin from '../models/Admin';
import jwt from '../helpers/jwt';

const login: RequestHandler = async (req, res) => {
    try {
        const { email, password } = req.body;
        const existingAdmin = await Admin.findOne({ email });
        if(!existingAdmin) {
            return res.status(statusCodes.UNAUTHORIZED).json({
                message: 'Invalid credentials'
            });
        }

        const isPasswordValid = await bcrypt.compare(password, existingAdmin.password);
        if(!isPasswordValid) {
            return res.status(statusCodes.UNAUTHORIZED).json({
                message: 'Invalid credentials'
            });
        }

        const token = jwt.generateToken({ adminId: existingAdmin._id, type: existingAdmin.type });
        res.json({
            message: 'Login successful',
            token
        });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

export default {
    login
};