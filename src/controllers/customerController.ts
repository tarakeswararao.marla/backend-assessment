import { RequestHandler } from 'express';
import bcrypt from 'bcryptjs';
import generator from 'generate-password';
import Customer from '../models/Customer';
import { statusCodes } from '../helpers/utils';
import jwt from '../helpers/jwt';

const create: RequestHandler = async (req, res) => {
    try {
        const { name, email, phoneNumber } = req.body;

        const existingCustomer = await Customer.findOne({ email });
        if(existingCustomer) {
            return res.status(statusCodes.BAD_REQUEST).json({
                message: 'Email already registered'
            });
        }
        // const password = generator.generate({
        //     length: 10,
        //     numbers: true
        // });
        const password = 'test@123';

        const customer = new Customer({
            name,
            email,
            phoneNumber,
            password: await bcrypt.hash(password, 10),
            textPassword: password
        });
        await customer.save();

        res.status(statusCodes.CREATED).json({
            message: 'Customer registered successfully',
            password
        });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
}

const get: RequestHandler = async (req, res) => {
    try {
        const customers = await Customer.find({});
        res.status(statusCodes.OK).json({ customers });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

const remove: RequestHandler = async (req, res) => {
    try {
        const customer = await Customer.findById(req.params.id);

        if(!customer) {
            return res.status(statusCodes.NOT_FOUND).json({
                message: 'Customer not found'
            });
        }

        await customer.deleteOne();

        res.json({ message: 'Customer removed successfully', customer });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

const login: RequestHandler = async (req, res) => {
    try {
        const { email, password } = req.body;

        const customer = await Customer.findOne({ email });
        if(!customer) {
            return res.status(statusCodes.UNAUTHORIZED).json({
                message: 'Invalid credentials'
            });
        }

        const isPasswordValid = await bcrypt.compare(password, customer.password);
        if(!isPasswordValid) {
            return res.status(statusCodes.UNAUTHORIZED).json({
                message: 'Invalid credentials'
            });
        }

        const token = jwt.generateToken({ customerId: customer._id, type: 'customer' });
        res.json({
            message: 'Login successful',
            token
        });
    } catch (error) {
        console.error(error);
        res.status(statusCodes.INTERNAL_SERVER_ERROR).json({
            message: 'Internal server error'
        });
    }
};

export default {
    create,
    get,
    remove,
    login
};