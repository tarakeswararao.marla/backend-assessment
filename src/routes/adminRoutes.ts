import { Router } from 'express';
import adminController from '../controllers/adminController';
import { validateLoginSchema } from '../middlewares/requestValidators';

const router = Router();

router.post(
    '/login',
    validateLoginSchema,
    adminController.login
);

export default router;