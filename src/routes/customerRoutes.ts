import { Router } from 'express';
import customerController from '../controllers/customerController';
import { validateCustomer, validateLoginSchema } from '../middlewares/requestValidators'
import authMiddleware from '../middlewares/authMiddleware';

const router = Router();

router.post(
    '/login',
    validateLoginSchema,
    customerController.login
);

router.post(
    '/create',
    authMiddleware(['admin']),
    validateCustomer,
    customerController.create
);

router.delete(
    '/:id',
    authMiddleware(['admin']),
    customerController.remove
);

router.get(
    '/',
    authMiddleware(['admin', 'delivery-partner']),
    customerController.get
);

export default router;