import { Router } from 'express';
import deliveryPartnerController from '../controllers/deliveryPartnerController';
import { validateDeliveryPartner, validateLoginSchema } from '../middlewares/requestValidators';
import authMiddleware from '../middlewares/authMiddleware';

const router = Router();

router.post(
    '/create',
    authMiddleware(['admin']),
    validateDeliveryPartner,
    deliveryPartnerController.create
);

router.post(
    '/login',
    validateLoginSchema,
    deliveryPartnerController.login
);

router.delete(
    '/:id',
    authMiddleware(['admin']),
    deliveryPartnerController.remove
);

router.get(
    '/',
    authMiddleware(['admin', 'customer']),
    deliveryPartnerController.get
);
export default router;