import { Router } from 'express';
import shipmentController from '../controllers/shipmentController';
import authMiddleware from '../middlewares/authMiddleware';
import { validateShipment, validateShipmentStatusUpdate } from '../middlewares/requestValidators';

const router = Router();

router.post(
    '/create',
    validateShipment,
    authMiddleware(['customer']),
    shipmentController.create
);
router.get(
    '/',
    authMiddleware(['customer', 'delivery-partner']),
    shipmentController.get
);
router.put(
    '/:id',
    authMiddleware(['delivery-partner']),
    validateShipmentStatusUpdate,
    shipmentController.update
);
router.delete(
    '/:id',
    authMiddleware(['customer']),
    shipmentController.remove
);

export default router;