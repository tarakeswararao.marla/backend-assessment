import app from './app';
import initDb from './loaders/initDb';

const port = process.env.PORT || 8080;
initDb().catch(e => {});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});