import { Request, Response, NextFunction } from 'express';
import Joi from '@hapi/joi';
import { statusCodes } from '../helpers/utils';

// Request validation schemas
const customerSchema = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  phoneNumber: Joi.string().required(),
});

const deliveryPartnerSchema = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  phoneNumber: Joi.string().required()
});

const loginSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required()
});

const shipmentSchema = Joi.object({
  customerId: Joi.string(),
  deliveryPartnerId: Joi.string().required(),
  sourceLocation: Joi.string().required(),
  targetLocation: Joi.string().required(),
});

const shipmentStatusUpdateSchema = Joi.object({
  status: Joi.string().valid('pending', 'in_progress', 'delivered').required(),
});

// =============================================================================================
// Request validation middleware functions
export const validateCustomer = (req: Request, res: Response, next: NextFunction) => {
  const { error } = customerSchema.validate(req.body);
  if (error) {
    return res.status(statusCodes.BAD_REQUEST).json({ message: error.details[0].message });
  }
  next();
};

export const validateDeliveryPartner = (req: Request, res: Response, next: NextFunction) => {
  const { error } = deliveryPartnerSchema.validate(req.body);
  if (error) {
    return res.status(statusCodes.BAD_REQUEST).json({ message: error.details[0].message });
  }
  next();
};

export const validateLoginSchema = (req: Request, res: Response, next: NextFunction) => {
  const { error } = loginSchema.validate(req.body);
  if (error) {
    return res.status(statusCodes.BAD_REQUEST).json({ message: error.details[0].message });
  }
  next();
};

export const validateShipment = (req: Request, res: Response, next: NextFunction) => {
  const { error } = shipmentSchema.validate(req.body);
  if (error) {
    return res.status(statusCodes.BAD_REQUEST).json({ message: error.details[0].message });
  }
  next();
};

export const validateShipmentStatusUpdate = (req: Request, res: Response, next: NextFunction) => {
  const { error } = shipmentStatusUpdateSchema.validate(req.body);
  if (error) {
    return res.status(statusCodes.BAD_REQUEST).json({ message: error.details[0].message });
  }
  next();
};
