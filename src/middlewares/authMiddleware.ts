import { NextFunction, RequestHandler } from 'express';
import jwt from '../helpers/jwt';
import { statusCodes } from '../helpers/utils';

const authMiddleware = (userTypes: any): RequestHandler => (req, res, next) => {
    const token = req.header('Authorization')?.split(' ')[1];

    if(!token) {
        return res.status(statusCodes.UNAUTHORIZED).json({
            message: 'Unauthorized'
        });
    }

    try {
        const decoded = jwt.verifyToken(token) as {
            customerId?: string;
            deliveryPartnerId?: string;
            adminId?: string;
            type?: string;
        };

        if (decoded.adminId) {
            req.headers.adminId = decoded.adminId;
        } else if (decoded.customerId) {
            req.headers.customerId = decoded.customerId;
        } else if (decoded.deliveryPartnerId) {
            req.headers.deliveryPartnerId = decoded.deliveryPartnerId;
        } else {
            return res.status(statusCodes.UNAUTHORIZED).json({
                message: 'Invalid token'
            });
        }
        if(userTypes.length>0 && userTypes.indexOf(decoded.type) ===-1) {
            return res.status(statusCodes.FORBIDDEN).json({
                message: "You are not authorized to perform this action"
            });
        }

        next();
    } catch (error) {
        console.error(error);
        res.status(statusCodes.UNAUTHORIZED).json({
            message: 'Invalid token'
        });
    }
};

export default authMiddleware;