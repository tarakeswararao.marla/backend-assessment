import {
    Request, Response, NextFunction
} from 'express';

import { statusCodes } from '../helpers/utils';

export default (err: any, req: Request, res: Response, next: NextFunction) => {
    console.error(err.stack);
    res.status(statusCodes.INTERNAL_SERVER_ERROR).json({ message: 'Internal server error'});
};